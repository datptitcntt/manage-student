﻿using System;
using App.Domain.Entities;
using App.Infrastructure.EntityConfigurations;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;

namespace App.Infrastructure.DbContexts
{
    public class SchoolDbContext : DbContext
    {
        #region Constructor

        public SchoolDbContext(DbContextOptions dbContextOptions, IConfiguration configuration,
            IServiceProvider serviceProvider) : base(dbContextOptions)
        {
            _configuration = configuration;
            _serviceProvider = serviceProvider;
        }

        #endregion

        #region Properties

        private readonly IConfiguration _configuration;

        private readonly IServiceProvider _serviceProvider;

        #endregion

        #region Properties

        /// <summary>
        ///     List of students.
        /// </summary>
        public DbSet<Student> Students { get; set; }

        public DbSet<Subject> Subjects { get; set; }

        #endregion

        #region Methods

        /// <summary>
        /// <inheritdoc />
        /// </summary>
        /// <param name="modelBuilder"></param>
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfigurationsFromAssembly(typeof(StudentConfiguration).Assembly);
        }

        /// <summary>
        /// <inheritdoc />
        /// </summary>
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            base.OnConfiguring(optionsBuilder);
            var loggerFactory = _serviceProvider.GetRequiredService<ILoggerFactory>();
            
            optionsBuilder.UseLoggerFactory(loggerFactory);
        }

        #endregion
    }
}