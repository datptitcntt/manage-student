﻿using App.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace App.Infrastructure.EntityConfigurations
{
    public class SubjectConfiguration : IEntityTypeConfiguration<Subject>
    {
        #region Methods

        /// <summary>
        ///     Configure subject entity in database.
        /// </summary>
        /// <param name="entityTypeBuilder"></param>
        public virtual void Configure(EntityTypeBuilder<Subject> entityTypeBuilder)
        {
            entityTypeBuilder.HasKey(x => x.Id);
            entityTypeBuilder.Property(x => x.Id).IsRequired();
            entityTypeBuilder.Property(x => x.Name).IsRequired();

            entityTypeBuilder.HasOne(x => x.Student).WithMany(x => x.Subjects)
                .HasForeignKey(x => x.StudentId);
        }

        #endregion
    }
}