﻿using App.Domain.Entities;
using App.Domain.ValueObjects;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace App.Infrastructure.EntityConfigurations
{
    public class StudentConfiguration : IEntityTypeConfiguration<Student>
    {
        #region Methods

        /// <summary>
        /// Configure entity.
        /// </summary>
        /// <param name="entityTypeBuilder"></param>
        public void Configure(EntityTypeBuilder<Student> entityTypeBuilder)
        {;
            entityTypeBuilder.HasKey(x => x.Id);

            entityTypeBuilder.Property(x => x.Id).IsRequired();

            entityTypeBuilder.Property(x => x.Name).IsRequired();

            entityTypeBuilder.OwnsOne(x => x.Address)
                .Property(x => x.Latitude)
                .HasColumnName(nameof(AddressType.Latitude));

            entityTypeBuilder.OwnsOne(x => x.Address)
                .Property(x => x.Longitude)
                .HasColumnName(nameof(AddressType.Longitude));
        }

        #endregion
    }
}