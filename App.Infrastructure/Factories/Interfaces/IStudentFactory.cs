﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using App.Domain.Aggregates;
using App.Domain.ValueObjects;

namespace App.Infrastructure.Factories.Interfaces
{
    public interface IStudentFactory
    {
        #region Methods

        /// <summary>
        /// Add 
        /// </summary>
        /// <param name="name"></param>
        /// <param name="address"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        Task<StudentAggregate> AddStudentAsync(string name, AddressType address, CancellationToken cancellationToken = default (CancellationToken));

        /// <summary>
        /// Get all students
        /// </summary>
        /// <returns></returns>
        Task<List<StudentAggregate>> GetAllStudentsASync(CancellationToken cancellationToken = default(CancellationToken));

        #endregion
    }
}