﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using App.Domain.Aggregates;
using App.Domain.Entities;
using App.Domain.ValueObjects;
using App.Infrastructure.DbContexts;
using App.Infrastructure.Factories.Interfaces;
using AutoMapper;
using Microsoft.EntityFrameworkCore;

namespace App.Infrastructure.Factories
{
    public class StudentFactory : IStudentFactory
    {
        #region Properties

        /// <summary>
        /// Entity framework unit of work.
        /// </summary>
        private readonly SchoolDbContext _dbContext;

        private readonly IMapper _mapper;

        #endregion

        #region Constructor

        public StudentFactory(SchoolDbContext dbContext, IMapper mapper)
        {
            _dbContext = dbContext;
            _mapper = mapper;
        }

        #endregion

        #region Methods

        /// <summary>
        /// <inheritdoc />
        /// </summary>
        /// <param name="name"></param>
        /// <param name="address"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public virtual async Task<StudentAggregate> AddStudentAsync(string name, AddressType address, CancellationToken cancellationToken)
        {
            var student = new Student(Guid.NewGuid(), name, address);
            _dbContext.Students.Add(student);
            await _dbContext.SaveChangesAsync(cancellationToken);
            return _mapper.Map<Student, StudentAggregate>(student);
        }

        /// <summary>
        /// <inheritdoc />
        /// </summary>
        /// <returns></returns>
        public virtual async Task<List<StudentAggregate>> GetAllStudentsASync(CancellationToken cancellationToken)
        {
            var students = await _dbContext.Students.ToListAsync(cancellationToken);
            return _mapper.Map<List<Student>, List<StudentAggregate>>(students);
        }

        #endregion

    }
}