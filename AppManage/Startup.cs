﻿using App.API.Helpers;
using App.API.Validators;
using App.API.ViewModels;
using App.Infrastructure.DbContexts;
using App.Infrastructure.Factories;
using App.Infrastructure.Factories.Interfaces;
using AutoMapper;
using FluentValidation;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace App.API
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddCors();
            services.AddSingleton(Configuration);
            services.AddDbContext<SchoolDbContext>(options => options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection")));
           
            services.AddScoped<AbstractValidator<AddStudentViewModel>, AddStudentValidator>();
            services.AddScoped<IStudentFactory, StudentFactory>();

            // Auto Mapper Configurations
            var mappingConfig = new MapperConfiguration(options =>
            {
                options.AddProfile(new AutoMapperProfile()); ;
            });

            var mapper = mappingConfig.CreateMapper();
            services.AddSingleton(mapper);

            services.AddMvc();


        }

        // This method gets called by the runtime. Use this  method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
                app.UseDeveloperExceptionPage();
            else
                app.UseHsts();
            // global cors policy
            app.UseCors(x => x
                .AllowAnyOrigin()
                .AllowAnyMethod()
                .AllowAnyHeader());


            app.UseHttpsRedirection();
            app.UseMvc();
        }
    }
}