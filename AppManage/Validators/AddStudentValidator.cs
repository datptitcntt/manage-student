﻿using App.API.ViewModels;
using FluentValidation;

namespace App.API.Validators
{
    public class AddStudentValidator : AbstractValidator<AddStudentViewModel>
    {
        #region Constructor

        public AddStudentValidator()
        {
            RuleFor(x => x.Name).NotNull();
            RuleFor(x => x.Address).NotNull();
            RuleFor(x => x.Address.Longitude).Must(longitude => longitude > 0);
            RuleFor(x => x.Address.Latitude).Must(latitude => latitude > 0);
        }

        #endregion

    }
}