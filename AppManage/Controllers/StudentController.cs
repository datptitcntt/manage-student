﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using App.API.ViewModels;
using App.Infrastructure.Factories.Interfaces;
using FluentValidation;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace App.API.Controllers
{
    [Route("api/student")]
    [ApiController]
    public class StudentController : ControllerBase
    {
        #region Properties
        

        private readonly AbstractValidator<AddStudentViewModel> _addStudentValidator;

        /// <summary>
        /// Manipulate student information.
        /// </summary>
        private readonly IStudentFactory _studentFactory;

        #endregion


        #region Constructor

        public StudentController(
            IStudentFactory studentFactory,
            AbstractValidator<AddStudentViewModel> addStudentValidator)
        {
            _addStudentValidator = addStudentValidator;
            _studentFactory = studentFactory;
        }

        #endregion

        #region Methods

        [HttpGet]
        public virtual async Task<IActionResult> GetAllStudentsAsync()
        {
            var students = await _studentFactory.GetAllStudentsASync();
            return Ok(students);
        }
        
        /// <summary>
        /// Add a student into system.
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost("")]
        public async Task<IActionResult> AddStudentAsync([FromBody] AddStudentViewModel model)
        {
            var validationResult = _addStudentValidator.Validate(model);
            if (!validationResult.IsValid)
            {
                var errors = validationResult.Errors;
                if (errors == null || errors.Count < 1)
                    return BadRequest();

                return BadRequest(errors.Select(x => new KeyValuePair<string, string>(x.PropertyName, x.ErrorMessage)));
            }

            var student = await _studentFactory.AddStudentAsync(model.Name, model.Address);
            return Ok(student);
        }
        
        #endregion
    }
}