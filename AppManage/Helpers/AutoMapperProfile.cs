﻿using App.Domain.Aggregates;
using App.Domain.Entities;
using AutoMapper;

namespace App.API.Helpers
{
    public class AutoMapperProfile : Profile
    {
        #region Constructor

        public AutoMapperProfile()
        {
            CreateMap<Student, StudentAggregate>();
        }

        #endregion
    }
}