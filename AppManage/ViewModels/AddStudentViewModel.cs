﻿using App.Domain.ValueObjects;

namespace App.API.ViewModels
{
    public class AddStudentViewModel
    {
        #region Properties

        public string Name { get; set; }

        public AddressType Address { get; set; }

        public string Quality { get; set; }

        #endregion

        #region Constructor

        public AddStudentViewModel(string name, AddressType address, string quality)
        {
            Name = name;
            Address = address;
            Quality = quality;
        }

        #endregion
    }
}