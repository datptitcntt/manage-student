﻿using System;
using System.Collections.Generic;

namespace App.API.ViewModels
{
    public class EditMultipleStudentViewModel
    {
        #region Properties

        public HashSet<Guid> StudentIds { get; set; }

        #endregion
    }
}