﻿using System;

namespace App.Domain.Entities
{
    public class Subject
    {
        public Subject()
        {
        }

        public Subject(Guid id, string name, Guid studentId)
        {
            Id = id;
            Name = name;
            StudentId = studentId;
        }
        
        #region Properties

        public Guid Id { get; set; }

        public string Name { get; set; }

        public Guid StudentId { get; set; }

        #endregion

        #region Navigation property

        public virtual Student Student { get; set; }

        #endregion
    }
}