﻿using System;
using System.Collections.Generic;
using App.Domain.ValueObjects;

namespace App.Domain.Entities
{
    public class Student
    {
        #region Properties

        public Guid Id { get; set; }

        public string Name { get; set; }

        public AddressType Address { get; set; }

        public string Quality { get; set; }
        

        #endregion

        #region Constructor

        public Student()
        {
            Subjects = new HashSet<Subject>();
        }

        public Student(Guid id, string name, AddressType address): this()
        {
            Id = id;
            Name = name;
            Address = address;
        }

        #endregion

        #region Navigation properties
        
        public virtual ICollection<Subject> Subjects { get; }

        #endregion
    }
}
