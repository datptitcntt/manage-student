﻿namespace App.Domain.ValueObjects
{
    public class AddressType
    {
        #region Properties

        public double Latitude { get; private set; }

        public double Longitude { get; private set; }

        #endregion

        #region Constructor
        
        public AddressType(double latitude, double longitude)
        {
            Latitude = latitude;
            Longitude = longitude;
        }

        #endregion
    }
}