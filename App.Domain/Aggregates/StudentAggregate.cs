﻿using System;
using App.Domain.ValueObjects;

namespace App.Domain.Aggregates
{
    public class StudentAggregate
    {
        #region Properties

        public Guid Id { get; }

        public string Name { get; }

        public AddressType Address { get; }
        
        #endregion

        #region Constructor

        public StudentAggregate(Guid id, string name, AddressType address)
        {
            Id = id;
            Name = name;
            Address = address;
        }

        #endregion
    }
}